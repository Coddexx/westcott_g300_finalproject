﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
public class RaycastSelect : MonoBehaviour {
	public HUDManager hudManager;
	public Animator doorAnim;
	public Animator doorAnim2;
	public GameObject stove;
	public GameObject stovefire;
	public bool stoveOn = false;
	public mouseExample mouseExample;

	public Renderer stoveRenderer;
	/*
	public Renderer cookingBacon1;
	public Renderer cookingBacon2;
	public Renderer cookingBacon3;
	public Renderer cuttingBoardTomato;
	public Renderer cutTomato;
	public Renderer cuttingBoardLettuce;
	public Renderer cutLettuce;
	public Renderer toastedBread;

	public Renderer sandwichBreadBottom;
	public Renderer sandwichTomato;
	public Renderer sandwichLettuce;
	public Renderer sandwichBacon1;
	public Renderer sandwichBacon2;
	public Renderer sandwichBacon3;
	public Renderer sandwichBreadTop;
	*/

	public GameObject cookingBacon;
	public GameObject cuttingBoardTomato;
	public GameObject cutTomato;
	public GameObject cuttingBoardLettuce;
	public GameObject cutLettuce;
	public GameObject toastedBread;
	public GameObject sandwichBreadBottom;
	public GameObject sandwichTomato;
	public GameObject sandwichLettuce;
	public GameObject sandwichBacon;
	public GameObject sandwichBreadTop;

	public AudioSource chopSound;
	public AudioSource panSizzle;
	public AudioSource DoorOpen;
	public AudioSource DoorClose;

	public bool hasBread;
	public bool hasTomato;
	public bool hasLettuce;
	public bool hasBacon;
	public bool hasKnife;
	public bool baconCooked;
	public bool tomatoCut;
	public bool lettuceCut;
	public bool breadToasted;

	public bool bottomPlaced;
	public bool baconPlaced;
	public bool tomatoPlaced;
	public bool lettucePlaced;
	public bool topPlaced;
	// Use this for initialization
	void Start () {
		//hudManager = GetComponent<HUDManager> ();
		//doorAnim = GetComponent<Animator>();
		stoveRenderer.sharedMaterial.color = Color.white;
		//cookingBaconCollider = cookingBaconObject.GetComponent<BoxCollider> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (topPlaced && baconPlaced && tomatoPlaced && lettucePlaced) {
			hudManager.sandwichFinished = true;
		} 



		RaycastHit hit;
		if (Physics.Raycast (transform.position, transform.forward, out hit, 100)) {

			//selecting objects
			if( Input.GetKeyDown(KeyCode.Mouse0)){
				//print ("clicked!"+ hit.transform.name);

				//item pickup
				if (hit.transform.tag == "Pickup" && mouseExample.scrollNum == 0) {
					//print ("Picked Up");
					if (hit.transform.name == "bread") {
						hudManager.slotitems [0].enabled = true;
						hasBread = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "tomato") {
						hudManager.slotitems [1].enabled = true;
						hasTomato = true;
						Destroy (hit.transform.gameObject);
					}

					if (hit.transform.name == "lettuce") {
						hudManager.slotitems [2].enabled = true;
						hasLettuce = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "bacon") {
						hudManager.slotitems [3].enabled = true;
						hasBacon = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "knife") {
						hudManager.slotitems [4].enabled = true;
						hasKnife = true;
						Destroy (hit.transform.gameObject);
					}

					if (hit.transform.name == "cookingBacon" && baconCooked) {
						hudManager.slotitems [5].enabled = true;
						hasBacon = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "cutTomato" && tomatoCut) {
						hudManager.slotitems [6].enabled = true;
						hasTomato = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "cutLettuce" && lettuceCut) {
						hudManager.slotitems [7].enabled = true;
						hasLettuce = true;
						Destroy (hit.transform.gameObject);
					}
					if (hit.transform.name == "toastedBread" && breadToasted) {
						hudManager.slotitems [8].enabled = true;
						hasBread = true;
						Destroy (hit.transform.gameObject);
					}
				}

				


				//fridge animations
				if (hit.transform.tag == "door") {
					if (doorAnim.GetBool ("doorOpen") == false) {
						doorAnim.SetBool ("doorOpen", true);
						DoorOpen.Play();
					} 
					else {
						doorAnim.SetBool ("doorOpen", false);
						DoorClose.Play ();
					}
				}

				if (hit.transform.tag == "door2") {
					if (doorAnim2.GetBool ("doorOpen") == false) {
						doorAnim2.SetBool ("doorOpen", true);
						DoorOpen.Play ();
					} 
					else {
						doorAnim2.SetBool ("doorOpen", false);
						DoorClose.Play ();
					}
						}
			
				// stove on off

				if (hit.transform.tag == "stove") {

					if (stoveOn == false) {
						stoveRenderer.sharedMaterial.color = Color.red;
						stovefire.SetActive(true);
						stoveOn = true;
						panSizzle.Play ();

					}
					else if (stoveOn == true) {
						stoveRenderer.sharedMaterial.color = Color.white;
						stovefire.SetActive(false);
						stoveOn = false;
						panSizzle.Stop ();
					}

					} 
			// cooking bacon
				if (hit.transform.name == "fryingPan" && mouseExample.scrollNum == 2 && hasBacon == true) {
					//print (" bacon cook");
					cookingBacon.SetActive(true);

					hasBacon = false;

					baconCooked = true;
					hudManager.slotitems [3].enabled = false;

					}
		//cutting tomato
				if (hit.transform.name == "cuttingBoard" && mouseExample.scrollNum == 4 && hasTomato == true) {
					//print (" tomato placed");
					cuttingBoardTomato.SetActive(true);
					hasTomato = false;

					hudManager.slotitems [1].enabled = false;
					}
			
				if (hit.transform.name == "cuttingBoardTomato" && mouseExample.scrollNum == 1 && hasKnife) {
					//print ("Tomato Cut");
					Destroy (hit.transform.gameObject);
					cutTomato.SetActive(true);
					tomatoCut = true;
					chopSound.Play ();

					}
			// cut lettuce
				if (hit.transform.name == "cuttingBoard" && mouseExample.scrollNum == 3 && hasLettuce == true) {
					//print (" lettuce placed");
					cuttingBoardLettuce.SetActive(true);
					hasLettuce = false;

					hudManager.slotitems [2].enabled = false;
				}

				if (hit.transform.name == "cuttingBoardLettuce" && mouseExample.scrollNum == 1 && hasKnife) {
					//print ("lettuce Cut");
					Destroy (hit.transform.gameObject);
					cutLettuce.SetActive(true);
					lettuceCut = true;
					chopSound.Play ();


				}
			

		//toast bread
				if (hit.transform.name == "toaster" && mouseExample.scrollNum == 5 && hasBread== true) {
					//print (" bread placed");
					toastedBread.SetActive(true);
					hasBread = false;
					breadToasted = true;
					hudManager.slotitems [0].enabled = false;
				}







		//Build Sandwich

				if (hit.transform.name == "plate") {
					
					if (mouseExample.scrollNum == 5 && hasBread ) {
						if (bottomPlaced ) {
							sandwichBreadTop.SetActive(true);
							topPlaced = true;
						}
						sandwichBreadBottom.SetActive(true);
						bottomPlaced = true;
					}
						

					if (mouseExample.scrollNum == 4 && hasTomato&& tomatoCut) {
						sandwichTomato.SetActive(true);
						tomatoPlaced = true;
					}

					if (mouseExample.scrollNum == 3 && hasLettuce&& lettuceCut) {
						sandwichLettuce.SetActive(true);
						lettucePlaced = true;
					}

					if (mouseExample.scrollNum == 2 && hasBacon&& baconCooked) {
						sandwichBacon.SetActive(true);
						baconPlaced = true;

					}


				
				}











				}
			}
		
		
		
		}
	
	
	}









