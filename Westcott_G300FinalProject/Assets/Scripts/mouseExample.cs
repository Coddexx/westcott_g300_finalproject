﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mouseExample : MonoBehaviour {
	int index =0;
	public int scrollNum =0;
	public Image[] slotShader;
	public Color idleColor;
	public Color activeColor;

	// Use this for initialization
	void Start () {
		for(int i=0; i<slotShader.Length; i++){
			slotShader[i].color = idleColor;
		}
		slotShader[scrollNum].color = activeColor;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis ("Mouse ScrollWheel") > .9f && scrollNum <5) {
			scrollNum++;
			//print (scrollNum);
			SlotActivate ();

	}
		if (Input.GetAxis ("Mouse ScrollWheel") < -.9f && scrollNum > 0) {
			scrollNum--;		
			//print(scrollNum);
			SlotActivate ();
		}
		//print (Input.GetAxisRaw ("Mouse ScrollWheel"));
	}



	void SlotActivate() {
		
		for(int i=0; i<slotShader.Length; i++){
			slotShader[i].color = idleColor;
	}

		slotShader[scrollNum].color = activeColor;


	}
}