﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {



	public Image[] slotitems;
	public GameObject tutorialWindow;
	public GameObject victoryWindow;

	public float timer = 10f;
	public bool sandwichFinished;

	void Start(){
		
	}
	
	// Update is called once per frame
	void Update () {
		if (timer >= 0) {
			timer = timer - Time.deltaTime;
		}
		if (timer <= 0) {
			tutorialWindow.SetActive (false);

			if (sandwichFinished) {
				victoryWindow.SetActive(true);

			}
		}
	}
}
