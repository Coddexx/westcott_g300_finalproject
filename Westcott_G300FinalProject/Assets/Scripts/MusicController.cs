﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
	AudioSource aC;




	// Use this for initialization
	void Start () {
		aC = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			aC.Stop();
		}
		float volume = aC.volume;
		volume -= Time.deltaTime * .1f;
		aC.volume = volume;

		aC.pitch = Random.Range (.8f, 1.2f);
		aC.volume = Random.Range (.8f, 1f);
	}
}
